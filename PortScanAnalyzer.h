//
// Created by KJ Patterson on 4/18/17.
//

#ifndef INC_1440_ITAK_PORTSCANANALYZER_H
#define INC_1440_ITAK_PORTSCANANALYZER_H
#include "Analyzer.h"
#include "Configuration.h"
#include "ResultSet.h"
#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <set>


class PortScanAnalyzer : public Analyzer
{
public:
   virtual ResultSet run(std::istream& myFile);

};


#endif //INC_1440_ITAK_PORTSCANANALYZER_H
