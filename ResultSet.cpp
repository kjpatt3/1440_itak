//
// Created by KJ Patterson on 4/18/17.
//

#include "ResultSet.h"
#include <fstream>
#include <iostream>

void ResultSet::display(std::ostream& FileOut, ResultSet obj)
{
    for (int i = 0; i < 10; i++)
    {
        FileOut << obj.getKey() << std::endl;
        for (int j = 0; j < 10; j++)
        {
            FileOut << obj.getValues()[j] << std::endl;
        }
    }
}