//
// Created by KJ Patterson on 4/18/17.
//

#ifndef INC_1440_HACKERDETECTION_ANALYZER_H
#define INC_1440_HACKERDETECTION_ANALYZER_H
#include <iostream>
#include "ResultSet.h"
#include "Configuration.h"


class Analyzer
{
public:
    virtual ResultSet run(std::istream&) = 0;
    std::string getConfiguration(std::string key){return m_configuration.getByKey(key).getValue();}
private:
    Configuration m_configuration;
    void setConfig(Configuration obj)
    {
        m_configuration = obj;
    };

};


#endif //INC_1440_HACKERDETECTION_ANALYZER_H
