//
// Created by KJ Patterson on 4/3/17.
//

#ifndef INC_1440_GENERICSDICTIONARY_DICTIONARY_H
#define INC_1440_GENERICSDICTIONARY_DICTIONARY_H
#include "KeyValue.h"
#include <iostream>


const int DEFAULT_SIZE = 10;

template <typename K, typename V>
class Dictionary
{
private:
    KeyValue<K,V>*  m_KeyValuePairs;
    unsigned int    m_allocated=0;
    unsigned int    m_count=0;


public:
    Dictionary<K,V>(unsigned int);
    Dictionary<K,V>();
    Dictionary<K,V>(const Dictionary &obj);
    ~Dictionary();
    void add(K,V);
    unsigned int getCount() const { return m_count; }
    KeyValue<K,V>getByKey(K key);
    const KeyValue<K,V> getByIndex(int) const;
    void remove(K key);
    void remove(unsigned int index);
    void print(std::ostream &out);

private:
    void grow();
};


//Constructor given a K, V
template <typename K, typename V>
Dictionary<K,V>::Dictionary()
{

    m_allocated = DEFAULT_SIZE;
    m_KeyValuePairs = new KeyValue<K,V>[m_allocated];
    m_count = 0;
};

//Constructor given a size
template <typename K, typename V>
Dictionary<K,V>::Dictionary(unsigned int size)
{
    m_allocated = size;
    m_KeyValuePairs = new KeyValue<K,V>[m_allocated];
    m_count = 0;
};

//Copy Constructor for Dictionary KeyValue Pairs;
template <typename K,typename V>
Dictionary<K,V>::Dictionary(const Dictionary &obj)
{
    m_KeyValuePairs = obj.m_KeyValuePairs;
    m_allocated = obj.m_allocated;
    m_count = obj.m_count;
    for (int i = 0; i < m_allocated; i++)
    {
        m_KeyValuePairs[i] = obj.m_KeyValuePairs[i];
    }
};

//Destructor
template <typename K, typename V>
Dictionary<K,V>::~Dictionary()
{
    if (m_KeyValuePairs != nullptr)
    {
        delete[] m_KeyValuePairs;
        m_KeyValuePairs = nullptr;
    }
}

//Add Function: Add's a KeyValue Pair to the Array
template <typename K, typename V>
void Dictionary<K,V>::add(K key, V value)
{
    KeyValue<K,V> element;
    element.setKey(key);
    element.setValue(value);
        if (m_allocated == 0)
        {
            m_allocated = DEFAULT_SIZE;
            m_KeyValuePairs = new KeyValue<K,V>[m_allocated];
        }

        if (m_count == m_allocated)
            grow();

            //Checking to see if key or value is already in array.
        for (int i = 0; i < m_count; i++)
        {
            if (key == m_KeyValuePairs[i].getKey() || value == m_KeyValuePairs[i].getValue())
            {
                throw std::invalid_argument( "Duplicate Key Found." );
            }
        }
    m_KeyValuePairs[m_count++] = element;
}

//Print a KeyValue Object
//Assuming that types K and V are able to use the << operator.
template<typename K, typename V>
void Dictionary<K,V>::print(std::ostream &out)
{
    for (int index=0; index < m_count; index++)
    {
       out << m_KeyValuePairs[index].getKey() << m_KeyValuePairs[index].getValue()<<std::endl;
    }
};
//get a KeyValue Object by finding a Key
template <typename K, typename V>
KeyValue<K,V> Dictionary<K,V>::getByKey(K key)
{
    for (int i = 0; i < m_count; i++)
    {
        if (key == m_KeyValuePairs[i].getKey())
        {
            return m_KeyValuePairs[i];
        }
    }
    // Throw an exception if value not found.
    throw std::invalid_argument( "Key Not Found." );

}

template<typename K, typename V>
const KeyValue<K,V> Dictionary<K,V>::getByIndex(const int index) const
{
    KeyValue<K,V> result;
    if (m_count > 0 && index < m_count)
    {
        result = m_KeyValuePairs[index];
    }
    return result;
}

//Remove a KeyValue Object from the array by Index
template <typename K, typename V>
void Dictionary<K,V>::remove(unsigned int index)
{
    if (index < m_count)
    {
        for (int i=index; i<m_count-1; i++)
        {
            //delete m_KeyValuePairs[index];
            m_KeyValuePairs[index] = m_KeyValuePairs[index+1];
        }
        m_count--;
    }
    else
    {
        throw std::invalid_argument("Invalid Index");
    }
}

// Remove a KeyValue Object by Key
template <typename K, typename V>
void Dictionary<K,V>::remove(K key)
{
    int indexFound = -1;
    for (int i = 0; i < m_count && indexFound == -1; i++)
    {
        if (key == m_KeyValuePairs[i].getKey())
        {
            indexFound = i;
        }
    }
    if (indexFound >= 0) {
        remove(indexFound);
    }
    else
    {
        throw std::invalid_argument("Invalid Key");
    }
};

//Expand the Array to add more KeyValue Objects
template <typename K, typename V>
void Dictionary<K,V>::grow()
{
    m_allocated *= 2;
    KeyValue<K,V>* newPairs = new KeyValue<K,V>[m_allocated];

    for (int i=0; i<m_allocated; i++)
    {
        newPairs[i] = m_KeyValuePairs[i];
    }
    delete [] m_KeyValuePairs;

    m_KeyValuePairs = newPairs;
}

#endif //INC_1440_GENERICSDICTIONARY_DICTIONARY_H
