//
// Created by KJ Patterson on 4/18/17.
//

#include "PortScanAnalyzer.h"



ResultSet PortScanAnalyzer::run(std::istream &myFile)
{
    std::string ip;
    int srcPort;
    std::map<std::string, std::set<int>> data;
    std::map<std::string, std::set<int>>::iterator it;


    while (myFile.good())
    {
        myFile.ignore(50, ',');
        std::getline(myFile, ip, ',');
        myFile.ignore(50, ',');
        std::string src;
        std::getline(myFile, src, ',');

        srcPort = std::stoi(src);

        it = data.find(ip);
        if (it== data.end())
        {
            data.insert(std::make_pair(ip, std::set<int>()));
            data[ip].insert(srcPort);
        }
        else
        {
            it->second.insert(srcPort);
        }
    }

    //Analyzing Portion
   int likelyThreshold =  std::stoi(getConfiguration("Likely Attack Port Count"));
   int possibleThreshold = std::stoi(getConfiguration("Possible Port Attack Count"));
   int portCount = std::stoi(getConfiguration("Port Count"));
    std::vector<std::string> likelyAttackers;
    std::vector <std::string> possibleAttackers;
    std::string LA = "LikelyAttackers";
    std::string PA = "PossibleAttackers";


    for (it = data.begin(); it != data.end(); it++)
    {
        if (it->second.size() >= likelyThreshold)
        {
            // IP is a likely attacker
            likelyAttackers.push_back(it->first);
        }
        else if (it->second.size() >= possibleThreshold && it->second.size() < likelyThreshold)
        {
            //IP is a possible attacker
            possibleAttackers.push_back(it->first);
        }
    }
    ResultSet obj;
    ResultSet obj1;
    std::map<std::string, std::vector<int>> newMap;
    std::map<std::string, std::vector<int>>::iterator iterator;
    obj.setKey(LA);
    obj.setValues(likelyAttackers);
    obj.setKey(PA);
    obj.setValues(possibleAttackers);
    return obj;

}
