//
// Created by KJ Patterson on 4/18/17.
//

#ifndef INC_1440_HACKERDETECTION_DENIALOFSERVICEANALYZER_H
#define INC_1440_HACKERDETECTION_DENIALOFSERVICEANALYZER_H
#include "Analyzer.h"
#include "Configuration.h"


class DenialOfServiceAnalyzer : public Analyzer
{
public:
    virtual ResultSet run(std::istream& File);
};


#endif //INC_1440_HACKERDETECTION_DENIALOFSERVICEANALYZER_H
