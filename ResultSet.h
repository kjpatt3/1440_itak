//
// Created by KJ Patterson on 4/18/17.
//

#ifndef INC_1440_HACKERDETECTION_RESULTSET_H
#define INC_1440_HACKERDETECTION_RESULTSET_H
#include <iostream>
#include <fstream>
#include <vector>
#include "PortScanAnalyzer.h"
#include "Dictionary.h"



class ResultSet : public Dictionary <std::string, std::vector <int>>
{
public:
    void display(std::ostream&, ResultSet);
std::string getKey() {return m_key;}
void setKey(std::string key) {m_key = key;}
std::vector <std::string> getValues() {return m_values;}
void setValues(std::vector<std::string> values) {m_values = values;}

private:
std::string m_key;
std::vector <std::string> m_values;

};


#endif //INC_1440_HACKERDETECTION_RESULTSET_H
