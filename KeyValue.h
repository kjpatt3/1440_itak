//
// Created by KJ Patterson on 4/3/17.
//

#ifndef INC_1440_GENERICSDICTIONARY_KEYVALUE_H
#define INC_1440_GENERICSDICTIONARY_KEYVALUE_H

template <typename K, typename V>

class KeyValue
{
public:
    K getKey(){return m_Key;}
    V getValue(){return m_Value;}



    void setKey(K key){m_Key = key;}
    void  setValue(V value){ m_Value = value;}


private:
    K m_Key;
    V m_Value;

};


#endif //INC_1440_GENERICSDICTIONARY_KEYVALUE_H
